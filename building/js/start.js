
// 1. Carousel slider
function projSlider(className, option) {
    $(className).owlCarousel(option);
}

// 2. Gallery hover
function galleryHover() {
    $('.h-gallery-itm').each(function(){
        $(this).hover(function(){
            $('.h-gallery-itm').removeClass('big').addClass('small');
            $(this).removeClass('small').addClass('big');
        }, function(){
            $(this).removeClass('big');
            $('.h-gallery-itm').removeClass('small');
        });
    });
}

// 3. Scroll show contact now button
function scrollShowBtn() {
    var window_width = $(window).width();

    if(window_width >= 1200) {
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 850) {
                $('.contact-now').addClass('shw');
            } else {
                $('.contact-now').removeClass('shw');
            }
        });
    }
}

// 4. Scroll to contact form
function scrollToFrm() {
    $('.contact-now button').on('click', function() {
        $('html, body').animate({
            scrollTop: $("#contact").offset().top
        }, 2000);
    });
}

// 5. Scroll fix menu
function scrollMenu() {
    $(document).scroll(function() {
        var y = $(this).scrollTop();
        if (y > 50) {
            $('.nav-header').addClass('fixed');
        } else {
            $('.nav-header').removeClass('fixed');
        }
    });
}

// 6. Scroll animation
function scrollAnimation(objScroll, objId, offset) {
    if(!$(objScroll).length) { return; }

    var w_scroll_window = 0;
    if (navigator.appVersion.indexOf("Win")!=-1) {
        w_scroll_window = 20;
    }
    if(($(window).width() + w_scroll_window) > 778 ) {  
      var animClass  = 'anima';

      var featureShow = new Waypoint({
        element: document.getElementById(objId),
        handler: function(direction) {
          if (direction === "down" && !$(objScroll).hasClass(animClass)) {
              $(objScroll).addClass(animClass);
          }
        }, offset: offset + '%'
      });
    } else {
      $(objScroll).addClass('anima');
    }
}

// 7. Init map
var map;
function intialize() {
    var latPos = $('#gg-map').data('lat');
    var lngPos = $('#gg-map').data('lng');

    var latlng = new google.maps.LatLng(latPos, lngPos);
    var myOptions = {
        zoom: 18,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    map = new google.maps.Map(document.getElementById("gg-map"), myOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: 'images/marker-1.png'
        // title: 'Hello World!'
    });

    var infoContent = '<div id="inf-content">'
                      + '<h3>Swanpak</h3>'
                      + '<p><strong>Địa chỉ: </strong>87 Hàm Nghi, Nguyễn Thái Bình, Quận 1, Hồ Chí Minh, Vietnam</p>'
                      + '<p><strong>Điện thoại: </strong>(+84) 962 2222 68</p>'
                      + '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: infoContent,
        maxWidth: 300
    });

    infowindow.open(map, marker);
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}


$(document).ready(function(){
    // Project carousel
    var window_width = $(window).width();

    var projOption = {
        items: (window_width <= 767) ? 1 : 3,
        margin: (window_width <= 767) ? 0 : 25,
        loop: true,
        autoWidth: (window_width <= 767) ? false : true,
        center: (window_width <= 767) ? false : true,
        // nav: true,
        autoplay: true,
        navText: ["<img src='images/arrow_left.png'>", "<img src='images/arrow_right.png'>"]
    };
    projSlider('.slide-thumb', projOption);

    // Charming carousel
    var charmingOption = {
        items: 1,
        loop: true,
        nav: false,
        autoplay: true,
    };
    projSlider('.charming-slider', charmingOption);

    // Gallery hover
    galleryHover();

    scrollShowBtn();
    scrollToFrm();

    // Scroll fix menu
    scrollMenu();

    intialize();
    // google.maps.event.addDomListener(window, "load", intialize);

    // Scroll animation
    scrollAnimation('.head-msg', 'head-msg', 60);
    scrollAnimation('.proj-slider', 'proj-slider', 30);
    scrollAnimation('.top-charming', 'top-charming', 70);
    scrollAnimation('.transportation', 'transportation', 60);
    scrollAnimation('.transportation-note', 'transportation-note', 100);
    scrollAnimation('.home-gallery', 'home-gallery', 50);
    scrollAnimation('.contact', 'contact', 50);
    
});