(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */

// Brand slider
function owlCarouselSlider () {
    var opt;
    var w_width = $(window).width();

    if(w_width > 768) {
        $("#brand-slider").owlCarousel({
            items: 5,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"]
        });
    } else if (w_width <= 768 && w_width > 520) {
        $("#brand-slider").owlCarousel({
            items: 3,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"]
        });
    } else if(w_width <= 520) {
        $("#brand-slider").owlCarousel({
            items: 2,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow.png'>", "<img src='images/right-arrow.png'>"]
        });
    }
    

}

// Product slider
function prodSlider(id) {
    var w_width = $(window).width();

    if(w_width > 480 && w_width <= 520) {
        $(id).owlCarousel({
            items: 2,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow-circle.png'>", "<img src='images/right-arrow-circle.png'>"]
        });
    } else if(w_width <= 480) {
        $(id).owlCarousel({
            items: 1,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow-circle.png'>", "<img src='images/right-arrow-circle.png'>"]
        });
    } else if(w_width <= 768 && w_width > 520){
        $(id).owlCarousel({
            items: 3,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow-circle.png'>", "<img src='images/right-arrow-circle.png'>"]
        });
    } else if(w_width > 768){
        $(id).owlCarousel({
            items: 4,
            margin: 30,
            autoplay: true,
            nav: true,
            navText: ["<img src='images/left-arrow-circle.png'>", "<img src='images/right-arrow-circle.png'>"]
        });
    }
    
}

// Product detail gallery
function prodDetailGallery() {
    $('#imageGallery').lightSlider({
        gallery:true,
        auto: true,
        item:1,
        loop:false,
        thumbItem:6,
        thumbMargin: 15,
        galleryMargin: 30,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide',
                thumbnail:true
            });
        }   
    });  
}

// Contact map
var map;
function intialize() {
    if($('#gg-map').length != 0) {
        var latPos = $('#gg-map').data('lat');
        var lngPos = $('#gg-map').data('lng');

        var latlng = new google.maps.LatLng(latPos, lngPos);
        var myOptions = {
            zoom: 14,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        map = new google.maps.Map(document.getElementById("gg-map"), myOptions);

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: 'images/tango-marker.png'
            // title: 'Hello World!'
        });

        var infoContent = '<div id="inf-content">'
                        + '<h3>TangoStore</h3><br/>'
                        + '<p><strong>Địa chỉ: </strong> 132 Cộng Hòa, phường 4, quận Tân Bình, Tp Hồ Chí Minh</p>'
                        + '<p><strong>Điện thoại: </strong> <a href="tel:(+84) 0978 275 605">(+84) 0978 275 605</a></p>'
                        + '<p><strong>Email: </strong> <a href="mailto:tangostore@gmail.com">tangostore@gmail.com</a></p>'
                        + '<p><strong>Website: </strong> <a href="tangostore.com">tangostore.com</a></p>'
                        + '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: infoContent,
            maxWidth: 300
        });

        infowindow.open(map, marker);
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    }
}

// Open search form 
function openSearch() {
    // Open form
    $('.search-btn').on('click', function(e){
        $(this).closest('.right-menu').siblings('.search-blk').addClass('shw');
    });

    // Close search form
    $('.close-search').on('click', function(e){
        $(this).closest('.search-blk').removeClass('shw');
    });
}

// Open filter
function openFilter() {
    $('.open-filter').on('click', function(e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.filt-ct').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).siblings('.filt-ct').addClass('shw');
        }
    });

    // Close filter
    $('.close-filter').on('click', function(e) {
        $(this).closest('.filt-ct').removeClass('shw');
        $(this).closest('.filt-ct').siblings('.open-filter').removeClass('active');
    });
}

// Responsive right menu
function rightMenu() {
    $('.right-mobile-btn').on('click', function(e) {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('.right-head-inner').removeClass('shw');
        } else {
            $(this).addClass('active');
            $(this).siblings('.right-head-inner').addClass('shw');

            $('.menu-content').removeClass('shw');
            $('.main-menu').removeClass('active');
        }
    });
}

// Responsive main menu
function responsiveMainMenu() {
    var w_width = $(window).width();

    if(w_width <= 990) {
        $('.main-menu').on('click', function(e) {
            if($(this).hasClass('active')) {
                $('.menu-content').removeClass('shw');
                $(this).removeClass('active');
            } else {
                $('.menu-content').addClass('shw');
                $(this).addClass('active');

                $('.right-mobile-btn').removeClass('active');
                $('.right-mobile-btn').siblings('.right-head-inner').removeClass('shw');
            }
        });
    }
}

// Toggle pc build list
function tooglePCList() {
    $('.choose-content .ttl').on('click', function(e) {
        if($(this).hasClass('closed')) {
            $(this).removeClass('closed');
            $(this).siblings('.choose-inner').removeClass('closed');
        } else {
            $(this).addClass('closed');
            $(this).siblings('.choose-inner').addClass('closed');
        }
    });
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    owlCarouselSlider();
    prodDetailGallery();
    intialize();
    openSearch();
    openFilter();

    // Product slider
    prodSlider('#mainboard-slide');
    prodSlider('#camera-mainboard-slide');
    prodSlider('#cpu-slide');
    prodSlider('#vga-slide');
    prodSlider('#camera-vga-slide');
    prodSlider('#ram-slide');
    prodSlider('#case-slide');
    prodSlider('#hdd-slide');
    prodSlider('#key-mouse-slide');
    prodSlider('#lcd-slide');
    prodSlider('#psu-slide');
    prodSlider('#ssd-slide');
    prodSlider('#arrival-prod-slide');
    prodSlider('#camera-arrival-prod-slide');
    prodSlider('#best-sell-slide');
    prodSlider('#on-sale-slide');
    prodSlider('#camera-on-sale-slide');
    prodSlider('#refer-prod');
    prodSlider('#related-prod');

    // Responsive right menu
    rightMenu();

    // Responsive main menu
    responsiveMainMenu();

    // Toggle pc build list
    tooglePCList();

    // Scroll top button
    $(window).scroll(function(){
		if ($(this).scrollTop() > 200) {
			$('.scroll-top').fadeIn();
			$('.fix-popup').fadeIn();
		} else {
            $('.scroll-top').fadeOut();
            $('.fix-popup').fadeOut();
		}
    });
    
    $('.scroll-top').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
